"""Data controller module with business logic and data manipulation tools."""

import random
import sqlite3

# SQL queries for data manipulation
CINE = 'CREATE TABLE IF NOT EXISTS lottery_table (id INTEGER PRIMARY KEY AUTOINCREMENT, first_name TEXT(50), last_name TEXT(50), email TEXT(50) );'
CHECK_IF_EXISTS = 'SELECT EXISTS(SELECT 1 FROM lottery_table WHERE email="{}");'
INSERT_USER = "INSERT INTO lottery_table(first_name, last_name, email) VALUES ('{}','{}','{}');"
GET_ALL = 'SELECT first_name, last_name, email FROM lottery_table;'


class AlreadyExistsException(Exception):
    """ Raised when user already registered"""
    pass


class NoActiveUsersError(Exception):
    """ Raised when there is no users to draw"""
    pass


class DataController:
    """
    Data controller that manages database connection and other business logic.
    """

    def __init__(self):
        self.conn = sqlite3.connect('lottery.db')

    def create_db(self):
        """
        Creates new database and required tables.
        """
        self.conn.execute(CINE)
        self.conn.commit()
        self.conn.close()

    def add_user(self, first_name, last_name, email):
        """
        Attempts to save new user into database.
        Check if user is already in database.
        If user is already registered, exception will be raised.

        :param first_name: First name of the user
        :param last_name: last name of the user
        :param email: Email of the user
        """
        check = self.conn.execute(CHECK_IF_EXISTS.format(email))
        self.conn.commit()
        is_exists = check.fetchone()
        check.close()
        if is_exists[0]:
            raise AlreadyExistsException('User already registered')
        self.conn.execute(INSERT_USER.format(first_name, last_name, email))
        self.conn.commit()
        self.conn.close()

    def get_all_users(self):
        """
        Fetches all user from database.
        """
        users = self.conn.execute(GET_ALL).fetchall()
        self.conn.commit()
        self.conn.close()
        return users

    def get_random_winner(self):
        """
        Uses fetched list of users to randomly choose one as a winner.

        :return: tuple with randomly chosen user.
        """
        users = self.get_all_users()
        if len(users) == 0:
            raise NoActiveUsersError
        winner = random.choice(users)
        return winner
