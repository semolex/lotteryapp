# Lottery app

### Simple lottery app that is able to register new users and randomly choose one as a winner.

#### Prerequisites
Create `virtualenv`

Install all required dependencies:

``pip install -r requirements.txt``

Create database for application:

``pythom create_db.py``

Start application:

``venv/bin/python -m flask run``
