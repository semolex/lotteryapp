"""Util to manually create db in case of need"""

from database import DataController

db = DataController()
db.create_db()
