"""Application that uses basic functionality to draw some registered user as a winner of lottery"""

from flask import Flask, render_template, request

from database import DataController, AlreadyExistsException, NoActiveUsersError

app = Flask(__name__)
app.debug = True


# helpers that are used as placeholders for confirmation of registration
confirmation_error = 'Your email is already part of the lottery participants!'
confirmation_success = 'You email is registered in our lottery! Good luck!'


@app.route('/')
def register():
    """
    Main route that renders registration form.
    """
    return render_template('index.html')


@app.route('/confirmation', methods=['POST'])
def confirm():
    """
    Confirmation page.
    Received request from registration page.
    Uses DataController to determine whether user already registered or new.
    Creates new user in database.
    Renders appropriate page with confirmation message.
    """
    first_name = request.form['first_name']
    last_name = request.form['last_name']
    user_email = request.form['user_email']
    db = DataController()
    try:
        db.add_user(first_name=first_name, last_name=last_name, email=user_email)
        return render_template('confirmation.html', first_name=first_name,
                               message=confirmation_success)
    except AlreadyExistsException:
        return render_template('confirmation.html', first_name=first_name,
                               message=confirmation_error)


@app.route('/results', methods=['GET'])
def results():
    """
    Renders page with lottery results.
    If there is a winner, page will contain all his attributes.
    If there is no registered users, appropriate message will be rendered.
    """
    db = DataController()
    try:
        winner = db.get_random_winner()
        return render_template('results.html', winner=winner)
    except NoActiveUsersError:
        return render_template('no_results.html')


if __name__ == '__main__':
    app.run()
